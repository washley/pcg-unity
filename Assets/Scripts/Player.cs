﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public float moveSpeedMultiplier = 5f;
    public float jumpImpulseMultiplier = 5f;
    public float jumpHeight = 5f;
    public float jumpImpulse = 5f;
    public float floatiness = 1f;

    public Rigidbody rb;
    public GameObject cam;

    private Vector2 moveForce;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
